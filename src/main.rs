extern crate enum_map;

mod agora;
mod nj;
mod grpc_server;

use agora::*;
use grpc_server::AgoraServer;
use grpc_server::grpc_agora::engine_service_server::*;
use nj::ComponentStore;
use tonic::transport::Server;
use std::env;
use std::sync::Arc;
use std::sync::Mutex;

// use agora::structures::*;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("Start");

    let args: Vec<String> = env::args().collect();

    if args.len() > 1 {
        println!("Running grpc");
        let addr = "127.0.0.1:50051".parse().unwrap();
        let service = AgoraServer::new(grpc_server::create_world());
    
        let serv = EngineServiceServer::new(service.clone());

        std::thread::spawn(move || {
            grpc_server::run_server(service);
        });
    
        println!("Starting server");
    
        Server::builder()
            .add_service(serv)
            .serve(addr)
            .await.unwrap();
    } else {
        println!("Running local");
        let service = AgoraServer::new(grpc_server::create_world());
        service.props.lock().unwrap().rate = 1000;
        grpc_server::run_server(service);
    };

    Ok(())
}
