pub use super::ChangeLog;

pub struct Tiles<T> {
    pub width: u16,
    pub height: u16,
    tiles: Vec<T>,
    change_log: ChangeLog<(u16,u16)>
}

impl <T> Tiles<T> where T: Default {

    pub fn new(width: u16, height: u16) -> Self {
        let tile_count = (width as usize) * (height as usize);
        let tiles = (0..tile_count).map(|_| T::default()).collect();
        let change_log = ChangeLog::new();
        return Tiles{width, height, tiles, change_log};
    }

}

impl <T> Tiles<T> {

    pub fn get(&self, x: &u16, y: &u16) -> &T {
        let row_offset = (*y as usize) * (self.width as usize);
        return &self.tiles[row_offset + (*x as usize)];
    }

    pub fn get_mut(&mut self, x: u16, y: u16) -> &mut T {
        let row_offset = (y as usize) * (self.width as usize);
        self.change_log.record_change((x,y));
        return &mut self.tiles[row_offset + (x as usize)];
    }

    pub fn read_changes<F>(&mut self, listener_id: u8, f: &mut F) where F: FnMut(&(u16,u16), &T)  {
        for pos in self.change_log.get_changes(listener_id) {
            let row_offset = (pos.1 as usize) * (self.width as usize);
            f(&pos, &self.tiles[row_offset + (pos.0 as usize)]);
        }
        self.change_log.progress_markers();
    }

}

impl <T> std::fmt::Debug for Tiles<T> where T: std::fmt::Debug {

    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let result = writeln!(f, "Tiles {{").and_then(|_r|{
            writeln!(f, "  size: {}x{}", self.width, self.height)
        }).and_then(|_r|{
            writeln!(f, "  tiles:")
        });
        
        if result.is_err() {
            return result;
        }

        // for x in 0..self.width {
        //     for y in 0..self.width {
        //         result = writeln!(f, "    ({:5},{:5}) = {:?}", x, y, self.get(&x,&y));
        //         if result.is_err() {
        //             return result;
        //         }
        //     }
        // }

        return write!(f, "}}");
    }

}