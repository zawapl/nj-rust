use crate::nj::GenerationalId;
use crate::nj::System;
use enum_map::EnumMap;

const INDEX_MASK: u64 = 0xff;

pub struct TimeWheel<E> where E: System {
    current_tick: u64,
    buckets: Vec<EnumMap<E, Vec<GenerationalId>>>
}

impl<E> TimeWheel<E> where E: System {

    pub fn new() -> TimeWheel<E> {
        return TimeWheel{
            current_tick: 0,
            buckets: (0..257).map(|_| EnumMap::new()).collect()
        };
    }

    pub fn tick(&mut self) -> EnumMap<E, Vec<GenerationalId>> {
        let scheduled = self.buckets.swap_remove((self.current_tick & INDEX_MASK) as usize);
        return scheduled;
    }

    pub fn push_empty_frames_update(&mut self, frame_updates: EnumMap<E, Vec<GenerationalId>>) {
        self.current_tick = self.current_tick.wrapping_add(1);
        self.buckets.push(frame_updates);
    }

    pub fn schedule(&mut self, delay: &u8, system: E, entity_id: GenerationalId) {
        debug_assert_ne!(delay, &0, "Must schedule in the future");
        println!("Scheduling delay={:?}, system={:?}, ids={:?}", delay, system, entity_id);
        let scheduled_tick = ((self.current_tick + *delay as u64) & INDEX_MASK) as usize;
        self.buckets[scheduled_tick][system].push(entity_id);
    }

    pub fn current_tick(&self) -> u64 {
        return self.current_tick;
    }

}

impl <E> std::fmt::Debug for TimeWheel<E> where E: System {

    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let result = writeln!(f, "TimeWheel {{").and_then(|_r|{
            writeln!(f, "  current_tick: {}", self.current_tick)
        }).and_then(|_r|{
            writeln!(f, "  buckets:")
        });
        
        if result.is_err() {
            return result;
        }

        // for i in 0..255 {
        //     if i == self.current_tick as usize {
        //         result = writeln!(f, " >> {:03} = {:?}", i, &self.buckets[i]);
        //     } else {
        //         result = writeln!(f, "    {:03} = {:?}", i, &self.buckets[i]);
        //     }
        //     if result.is_err() {
        //         return result;
        //     }
        // }

        return write!(f, "}}");
    }

}