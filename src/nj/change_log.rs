use super::*;
use std::collections::vec_deque::{VecDeque,Iter};

#[derive(Debug)]
pub struct ChangeLog<T> {
    marks: Vec<usize>,
    pub ids: VecDeque<T>
}

impl<T> ChangeLog<T> {

    pub fn new() -> Self {
        return ChangeLog{
            marks: vec!(1),
            ids: VecDeque::with_capacity(100)
        };
    }

    pub fn record_change(&mut self, id: T) {
        self.ids.push_back(id);
    }

    pub fn get_changes(&mut self, listener_id: u8) -> std::iter::Skip<Iter<'_, T>> {
        let listener_id = listener_id as usize;
        let iter = self.ids.iter().skip(self.marks[listener_id]);
        self.marks[listener_id] = self.ids.len();
        return iter;
    }

    pub fn progress_markers(&mut self) {
        let min = self.marks.iter().fold(usize::max_value(), |a,b| std::cmp::min(a,*b));
        for i in 0..self.marks.len() {
            self.marks[i] -= min;
        }
        if min > 0 {
            self.ids.drain(0..min);
        }
    }

}