use std::fmt;

const INDEX_MASK: u32 = 0xffffff;

#[derive(Hash,Eq,PartialEq,PartialOrd)]
pub struct GenerationalId {
    id: u32,
}

impl GenerationalId {

    pub fn new(index: u32) -> Self {
        let id = (index & INDEX_MASK) + INDEX_MASK + 1;
        return Self{id};
    }

    pub fn increment_generation(&mut self) {
        self.id += INDEX_MASK + 2;
    }

    pub fn generation(&self) -> u32 {
        return self.id & !INDEX_MASK;
    }

    pub fn normalized_generation(&self) -> u8 {
        return (self.generation() >> 24) as u8;
    }

    pub fn index(&self) -> u32 {
        return self.id & INDEX_MASK;
    }

    pub fn clone(&self) -> GenerationalId {
        return GenerationalId{id: self.id};
    }

    pub fn update_index(&mut self, index: u32) {
        self.id = (self.id & !INDEX_MASK) | index;
    }

}

impl fmt::Debug for GenerationalId {

    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "@{}:{}", self.normalized_generation(), self.index())
    }

}

impl core::cmp::Ord for GenerationalId {

    fn cmp(&self, other: &Self) -> core::cmp::Ordering {
        return self.index().cmp(&other.index());
    }

}