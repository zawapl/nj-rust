mod dense_store;

use super::GenerationalId;
use super::ChangeLog;

pub use dense_store::DenseStore;

pub trait ComponentStore<T> {

    fn set<F>(&mut self, id: &GenerationalId, f: F) where F: Fn(&mut T);

    fn get(&self, id: &GenerationalId) -> Option<&T>;
    
    fn get_mut(&mut self, id: &GenerationalId) -> Option<&mut T>;

    fn read_changes<F>(&mut self, listener_id: u8, f: &mut F) where F: FnMut(&GenerationalId, &T);

}