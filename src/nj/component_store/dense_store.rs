use super::ChangeLog;
use super::ComponentStore;
use super::GenerationalId;

#[derive(Debug, Default)]
struct Entry<T> {
    generation: u8,
    value: T,
}

pub struct DenseStore<T> {
    entries: Vec<Entry<T>>,
    change_log: ChangeLog<GenerationalId>,
}

impl<T> DenseStore<T> {
    pub fn new() -> Self {
        let entries = Vec::new();
        let change_log = ChangeLog::new();
        return Self {
            entries,
            change_log,
        };
    }
}

impl<T> ComponentStore<T> for DenseStore<T>
where
    T: Default + std::fmt::Debug,
{
    fn set<F>(&mut self, id: &GenerationalId, f: F)
    where
        F: Fn(&mut T),
    {
        let index = id.index() as usize;
        let len = self.entries.len();
        if len <= index {
            self.entries.resize_with(index + 1, Entry::default);
        }
        debug_assert!(
            self.entries[index].generation != id.normalized_generation(),
            "Attempted to override a component entry {:?}",
            self.entries[index]
        );
        self.entries[index].generation = id.normalized_generation();
        self.change_log.record_change(id.clone());
        f(&mut self.entries[index].value);
    }

    fn get(&self, id: &GenerationalId) -> Option<&T> {
        let index = id.index() as usize;

        if self.entries.len() > index {
            let entry = &self.entries[index];

            if entry.generation == id.normalized_generation() {
                return Some(&entry.value);
            }
        }

        return None;
    }

    fn get_mut(&mut self, id: &GenerationalId) -> Option<&mut T> {
        let index = id.index() as usize;

        if self.entries.len() > index {
            let entry = &mut self.entries[index];

            if entry.generation == id.normalized_generation() {
                self.change_log.record_change(id.clone());
                return Some(&mut entry.value);
            }
        }

        return None;
    }

    fn read_changes<F>(&mut self, listener_id: u8, f: &mut F)
    where
        F: FnMut(&GenerationalId, &T),
    {
        for id in self.change_log.get_changes(listener_id) {
            let index = id.index() as usize;

            if self.entries.len() > index {
                let entry = &self.entries[index];

                if entry.generation == id.normalized_generation() {
                    f(&id, &entry.value);
                }
            }
        }
        self.change_log.progress_markers();
    }
}

impl<T> std::fmt::Debug for DenseStore<T>
where
    T: std::fmt::Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut result = writeln!(f, "DenseStore {{")
            .and_then(|_r| writeln!(f, "  change_log_len: {}", self.change_log.ids.len()))
            .and_then(|_r| writeln!(f, "  entries: "));

        if result.is_err() {
            return result;
        }

        for i in 0..self.entries.len() {
            result = writeln!(f, "    {:?}", self.entries[i]);

            if result.is_err() {
                return result;
            }
        }

        return write!(f, "}}");
    }
}
