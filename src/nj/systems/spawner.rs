use super::*;

pub fn process<E,T,C,S>(ids: Drain<GenerationalId>, event_listeners: &E, world: &mut World<T,S,C>) where E: EventListeners<T,S,C>, S: System {
    let mut init_queue = Vec::new();

    for id in ids {
        if let Some(spawner) = world.spawners.get_mut(&id) {
            let allocator = &world.allocator;
            spawner.units.retain(|unit_id| allocator.is_alive(unit_id));

            if spawner.units.len() < spawner.max_count as usize {
                let unit_id = world.allocator.allocate();
                init_queue.push((spawner.unit_template, unit_id.clone()));
                spawner.units.push(unit_id);
            }

            world.actions.schedule(&spawner.delay, AgoraSystem::TriggerSpawner, vec!(id));

        } else {
            debug_assert!(false, "Spawner got deleted {:?}", id);
        }
    }

    for (template,id) in init_queue {
        init(template, id, &event_listeners, world);
    }
}

fn init(template: SpawnerUnitTemplate, id: GenerationalId, event_listeners: &EventListeners, world: &mut AgoraWorld) {
    use SpawnerUnitTemplate::*;
    match template {
        Wolf(x,y) => initialisers::wolf::new(id, x, y, &event_listeners, world),
        _ => panic!("Failed to resolve SpawnerUnitTemplate for {:?}", template)
    };
}