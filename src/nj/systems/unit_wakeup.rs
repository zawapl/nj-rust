use super::*;

pub fn process<E,T,S,C>(ids: Drain<GenerationalId>, event_listeners: &E, world: &mut World<T,S,C>) where E: EventListeners<T,S,C>, S: System {
    for id in ids {
        if !world.allocator.is_alive(&id) {
            continue;
        }

        event_listeners.on_wake_up(&id, world);
    }
}