// mod idler;
// mod spawner;
// mod unit_step;
// mod unit_wakeup;

use crate::nj::GenerationalId;
// use crate::nj::World;
// use crate::nj::EventListeners;
use enum_map::Enum;

pub trait System: Enum<std::vec::Vec<GenerationalId>> + std::fmt::Debug {

}