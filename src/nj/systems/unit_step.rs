use super::*;
use std::collections::HashMap;
use crate::nj::pathfinding;

pub fn process<E,T,S,C>(ids: Drain<GenerationalId>, event_listeners: &E, world: &mut World<T,S,C>) where E: EventListeners<T,S,C>, S: System {
    let mut to_reschedule = HashMap::new();

    for id in ids {
        if !world.allocator.is_alive(&id) {
            continue;
        }

        if world.units.get(&id).unwrap().arrival_time != world.calendar.tick {
            continue;
        }

        event_listeners.on_arrival(&id, world);

        let unit = world.units.get_mut(&id).unwrap();

        if let Some(target_pos) = unit.path.pop() {
            let target_tile = world.tiles.get_mut(target_pos.0, target_pos.1);
            if target_tile.terrain_flags.contains(&unit.terrain_flags) {
                let arrival_delay = pathfinding::dist(&unit.position, &target_pos) as u8 * unit.travel_time;

                world.tiles.get_mut(unit.position.0,unit.position.1).entities.remove(&id);
                world.tiles.get_mut(target_pos.0, target_pos.1).entities.insert(id.clone());

                unit.position = target_pos;

                to_reschedule.entry(arrival_delay).or_insert_with(Vec::new).push(id);
            } else {
                event_listeners.on_blocked_path(&id, world);
            }
        } else {
            event_listeners.on_end_of_path(&id, world);
        }
    }

    for (delay,ids) in to_reschedule.drain() {
        world.actions.schedule(&delay, AgoraSystem::UnitStep, ids);
    }
}