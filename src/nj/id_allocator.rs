use super::GenerationalId;
use super::ChangeLog;

pub struct IdentityAllocator<T> {
    entries: Vec<(GenerationalId,T)>,
    next_available: u32,
    available_count: u32,
    change_log: ChangeLog<GenerationalId>
}

impl <T> IdentityAllocator<T> {

    pub fn new() -> IdentityAllocator<T> {
        let entries = Vec::new();
        let next_available = 0;
        let available_count = 0;
        let change_log = ChangeLog::new();
        return Self{entries, next_available, available_count, change_log};
    }

    pub fn allocate(&mut self, item: T) -> GenerationalId {
        if self.available_count > 0 {
            self.available_count -= 1;
            let next_index = self.next_available;
            let entry = &mut self.entries[next_index as usize];
            self.next_available = entry.0.index();
            entry.0.update_index(next_index);
            entry.1 = item;
            self.change_log.record_change(entry.0.clone());
            return entry.0.clone();
        } else {
            let pos = self.entries.len();
            let id = GenerationalId::new(pos as u32);
            self.entries.push((id.clone(), item));
            self.change_log.record_change(id.clone());
            return id;
        }
    }

    pub fn free(&mut self, id: GenerationalId) {
        let index = id.index();
        let entry = &mut self.entries[index as usize];
        debug_assert_eq!(entry.0.generation(), id.generation(), "Tried to double delete");
        entry.0.increment_generation(); // Increment generation
        entry.0.update_index(self.next_available); // Point to next_avilable entry
        self.next_available = index; // Make next_available point to this one
        self.available_count += 1;
        self.change_log.record_change(id);
    }

    pub fn get(&self, id: &GenerationalId) -> &T {
        let index = id.index();
        let entry = &self.entries[index as usize];
        debug_assert_eq!(entry.0.generation(), id.generation(), "Tried to get for a deleted entry");
        return &entry.1;
    }

    // pub fn is_alive(&self, id: &GenerationalId) -> bool {
    //     let index = id.index() as usize;
    //     return (index < self.entries.len()) && (self.entries[index].generation() == id.generation());
    // }

    pub fn read_changes<F>(&mut self, listener_id: u8, f: &mut F) where F: FnMut(u32, &T) {
        for id in self.change_log.get_changes(listener_id) {
            let index = id.index() as usize;
            let entry = self.entries[index];
            let item = &entry.1;
            f(id.index(), item);
        }
        self.change_log.progress_markers();
    }

}

impl <T> std::fmt::Debug for IdentityAllocator<T> where T: std::fmt::Debug {

    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {

        // let mut dead: Vec<bool> = (0..self.entries.len()).map(|_entry| {false}).collect();
        // let mut next = self.next_available as usize;
        // for _i in 0..self.available_count {
        //     dead[next] = true;
        //     next = self.entries[next].0.index() as usize;
        // }

        let result = writeln!(f, "IndexAllocator {{").and_then(|_r|{
            writeln!(f, "  next_available: {}", self.next_available)
        }).and_then(|_r|{
            writeln!(f, "  available_count: {}", self.available_count)
        }).and_then(|_r|{
            writeln!(f, "  entries:")
        });

        if result.is_err() {
            return result;
        }

        // for i in 0..self.entries.len() {
        //     result = write!(f, "    {:?}", self.entries[i]).and_then(|r| {
        //         if dead[i] {
        //             writeln!(f, "XXX")
        //         } else {
        //             writeln!(f, "")
        //         }
        //     });
            
        //     if result.is_err() {
        //         return result;
        //     }
        // }

        return write!(f, "}}");
    }

}

#[cfg(test)]
mod test {

    use super::*;
    
    #[test]
    fn simple() {
        let mut store = IdentityAllocator::new();

        let i1 = store.allocate(11);

        assert_eq!(i1.index(), 0);
        assert_eq!(i1.normalized_generation(), 1);

        assert_eq!(store.get(&i1), &11);

        store.free(i1);

        let i2 = store.allocate(22);
        let i3 = store.allocate(33);

        assert_eq!(i2.index(), 0);
        assert_eq!(i2.normalized_generation(), 2);
        assert_eq!(store.get(&i2), &22);
        assert_eq!(i3.index(), 1);
        assert_eq!(i3.normalized_generation(), 1);
        assert_eq!(store.get(&i31), &33);
    }

}