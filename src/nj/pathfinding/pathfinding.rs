extern crate pathfinding;

use pathfinding::prelude::astar;

use crate::nj::Tiles;

pub fn find_path<T,F>(start: (u16,u16), end: (u16,u16), tiles: &Tiles<T>, filter: F) -> Option<Vec<(u16,u16)>> where F: Fn(&T) -> bool, {

    let sucessors = |position:&(u16,u16)| {
        let mut result = Vec::new();
        let x = position.0 as i32;
        let y = position.1 as i32;

        let possible = vec![(x-1,y-1),(x+0,y-1),(x+1,y-1),(x-1,y+0),(x+1,y+0),(x-1,y+1),(x+0,y+1),(x+1,y+1)];

        for (x,y) in possible {
            if (x >= 0) && (y >= 0) {
                let x = x as u16;
                let y = y as u16;
                if (x < tiles.width) && (y < tiles.height) && filter(tiles.get(&x,&y)) {
                    result.push(((x,y),dist(position,&(x,y))));
                }
            }
        }

        return result;
    };

    let heuristric = |position:&(u16,u16)| {
        return dist(position, &end);
    };

    let success = |position:&(u16,u16)| {
        return position.0 == end.0 && position.1 == end.1;
    };

    return astar(&start, sucessors, heuristric, success).map(|(mut path,_)|{
        path.reverse();
        path.pop();
        return path;
    });

}

pub fn dist(a: &(u16,u16), b: &(u16,u16)) -> u32 {
    let dx = if a.0 > b.0 {
        a.0 - b.0
    } else {
        b.0 - a.0
    } as u32;
    let dy = if a.1 > b.1 {
        a.1 - b.1
    } else {
        b.1 - a.1
    } as u32;
    if dx > dy {
        return 2*dx + dy;
    } else {
        return 2*dy + dx;
    }
}