
use super::components::*;
use super::systems::AgoraSystem;
use crate::nj::*;
use super::tiles::Tile;
use rand::rngs::StdRng;
use rand::SeedableRng;

#[derive(Debug)]
pub struct AgoraWorld {
    pub random: StdRng,
    pub tiles: Tiles<Tile>,
    pub actions: TimeWheel<AgoraSystem>,
    pub allocator: IdentityAllocator<UnitType>,
    pub spawners: DenseStore<Spawner>,
    pub regroup_position: DenseStore<RegroupPosition>,
    pub units: DenseStore<UnitBase>,
    pub idlers: DenseStore<Idler>,
}

impl AgoraWorld {

    pub fn new(width: u16, height: u16) -> AgoraWorld {
        return AgoraWorld {
            random: StdRng::seed_from_u64(11),
            tiles: Tiles::new(width, height),
            allocator: IdentityAllocator::new(),
            actions: TimeWheel::new(),
            spawners: DenseStore::new(),
            regroup_position: DenseStore::new(),
            units: DenseStore::new(),
            idlers: DenseStore::new(),
        };
    }

}