use crate::nj::GenerationalId;
use super::terrain_flags::TerrainFlags;
use std::collections::HashSet;

#[derive(Debug)]
pub struct Tile {
    pub entities: HashSet<GenerationalId>,
    pub terrain_flags: TerrainFlags
}

impl Tile {

    // pub fn verify(&self) {
    //     self.terrain_flags.verify();
    // }

}

impl Default for Tile {

    fn default() -> Self {
        return Tile {
            entities: HashSet::new(),
            terrain_flags: TerrainFlags::WALK.and(&TerrainFlags::BUILD_LAND)
        };
    }

}