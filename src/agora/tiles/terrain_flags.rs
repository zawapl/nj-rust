#[derive(Debug)]
pub struct TerrainFlags(pub u8);

impl TerrainFlags {
    
    pub const EMPTY       :TerrainFlags = TerrainFlags(0);
    pub const WALK        :TerrainFlags = TerrainFlags(1 << 0);
    // pub const ROAD        :TerrainFlags = TerrainFlags(1 << 1);
    // pub const SWIM        :TerrainFlags = TerrainFlags(1 << 2);
    // pub const MEADOW      :TerrainFlags = TerrainFlags(1 << 3);
    // pub const BUILD_WATER :TerrainFlags = TerrainFlags(1 << 4);
    pub const BUILD_LAND  :TerrainFlags = TerrainFlags(1 << 5);
    // pub const SHORE       :TerrainFlags = TerrainFlags(1 << 6);
    // pub const BUILD_AGORA :TerrainFlags = TerrainFlags(1 << 7);

    pub fn and(&self, other: &TerrainFlags) -> TerrainFlags {
        return TerrainFlags(self.0 | other.0);
    }

    // pub fn minus(&self, other: &TerrainFlags) -> TerrainFlags {
    //     return TerrainFlags(self.0 & !other.0);
    // }

    pub fn contains(&self, other: &TerrainFlags) -> bool {
        return (self.0 & other.0) == other.0;
    }

    // pub fn verify(&self) {

    // }

}