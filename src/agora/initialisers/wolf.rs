use super::*;

pub fn new(
    id: GenerationalId,
    x: u16,
    y: u16,
    event_listeners: &EventListeners,
    world: &mut AgoraWorld,
) {
    world.regroup_position.set(&id, |position| {
        position.x = x;
        position.y = y;
    });

    world.units.set(&id, |unit_base| {
        unit_base.terrain_flags = TerrainFlags::WALK;
        unit_base.travel_time = 2;
        unit_base.position = (x, y);
    });

    world.idlers.set(&id, |idler| {
        idler.wait_time = 2;
    });

    event_listeners.on_wake_up(&id, world);
}
