use super::*;

pub fn new(x: u16, y: u16, world: &mut AgoraWorld) -> GenerationalId {
    let id = world.allocator.allocate(UnitType::Wolf);

    world.spawners.set(&id, |spawner| {
        spawner.units = Vec::with_capacity(7);
        spawner.delay = 1;
        spawner.max_count = 1;
        spawner.unit_template = SpawnerUnitTemplate::Wolf(x,y);
    });

    world.tiles.get_mut(x, y).entities.insert(id.clone());

    world.actions.schedule(&1, AgoraSystem::TriggerSpawner, id.clone());

    return id;
}