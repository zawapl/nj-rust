use super::*;
use crate::nj::pathfinding;

pub fn process(
    ids: Drain<GenerationalId>,
    event_listeners: &EventListeners,
    world: &mut AgoraWorld,
) {
    for id in ids {
        if !world.allocator.is_alive(&id) {
            println!("Unit not alive {:?}", id);
            continue;
        }

        if world.units.get(&id).unwrap().arrival_time != (world.actions.current_tick()) {
            println!(
                "Step wrongly timed, expected {}, now is {}",
                world.units.get(&id).unwrap().arrival_time,
                (world.actions.current_tick())
            );
            continue;
        }

        event_listeners.on_arrival(&id, world);

        let unit = world.units.get_mut(&id).unwrap();

        unit.previous_position = unit.position;

        if let Some(target_pos) = unit.path.pop() {
            let target_tile = world.tiles.get_mut(target_pos.0, target_pos.1);
            if target_tile.terrain_flags.contains(&unit.terrain_flags) {
                let arrival_delay = pathfinding::dist(&unit.position, &target_pos) as u8 * unit.travel_time;

                world.tiles.get_mut(unit.position.0, unit.position.1).entities.remove(&id);
                world.tiles.get_mut(target_pos.0, target_pos.1).entities.insert(id.clone());

                unit.position = target_pos;
                unit.arrival_time = world.actions.current_tick() + arrival_delay as u64;
                unit.departure_time = world.actions.current_tick();

                world.actions.schedule(&arrival_delay, AgoraSystem::UnitStep, id);
            } else {
                event_listeners.on_blocked_path(&id, world);
            }
        } else {
            event_listeners.on_end_of_path(&id, world);
        }
    }
}
