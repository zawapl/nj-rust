mod spawner;
mod unit_step;
mod unit_wakeup;

use crate::agora::events::EventListeners;
use crate::nj::GenerationalId;
use crate::agora::AgoraWorld;
use crate::nj::*;
use crate::agora::components::*;
use enum_map::Enum;
use std::vec::Drain;

#[derive(Debug,Enum)]
pub enum AgoraSystem {
    TriggerSpawner,
    UnitWakeUp,
    UnitStep,
}

impl AgoraSystem {

    pub fn process(&self, ids: Drain<GenerationalId>, event_listeners: &EventListeners, world: &mut AgoraWorld) {
        println!("Processing {} events of {:?}", ids.len(), self);
        use AgoraSystem::*;
        match self {
            TriggerSpawner => spawner::process(ids, event_listeners, world),
            UnitWakeUp => unit_wakeup::process(ids, event_listeners, world),
            UnitStep => unit_step::process(ids, event_listeners, world)
        }
    }

}

impl System for AgoraSystem {
    
}