use super::*;

pub fn process(ids: Drain<GenerationalId>, event_listeners: &EventListeners, world: &mut AgoraWorld) {
    for id in ids {
        if !world.allocator.is_alive(&id) {
            continue;
        }

        event_listeners.on_wake_up(&id, world);
    }
}