mod idler;
mod regroup_wake_up;

use crate::nj::GenerationalId;
use crate::agora::AgoraWorld;
use crate::agora::systems::AgoraSystem;
use crate::nj::*;

type EventHandlerReference = &'static dyn Fn(&GenerationalId, &mut AgoraWorld); 

pub struct EventListeners {
    on_end_of_path: [EventHandlerReference; 1],
    on_arrival: [EventHandlerReference; 0],
    on_blocked_path: [EventHandlerReference; 1],
    on_wake_up: [EventHandlerReference; 1],
}

impl EventListeners {

    pub fn new() -> Self {
        return Self {
            on_end_of_path: [
                &idler::IdlerHandler::sleep
            ],
            on_arrival: [
            ],
            on_blocked_path: [
                &idler::IdlerHandler::sleep
            ],
            on_wake_up: [
                &regroup_wake_up::RegroupWakeUp::wake_up
            ]
        };
    }

    pub fn on_end_of_path(&self, id: &GenerationalId, world: &mut AgoraWorld) {
        // println!("on_end_of_path({:?})", id);
        for handler in &self.on_end_of_path {
            handler(&id, world);
        }
    }

    pub fn on_arrival(&self, id: &GenerationalId, world: &mut AgoraWorld) {
        // println!("on_arrival({:?})", id);
        for handler in &self.on_arrival {
            handler(&id, world);
        }
    }

    pub fn on_blocked_path(&self, id: &GenerationalId, world: &mut AgoraWorld) {
        // println!("on_blocked_path({:?})", id);
        for handler in &self.on_blocked_path {
            handler(&id, world);
        }
    }

    pub fn on_wake_up(&self, id: &GenerationalId, world: &mut AgoraWorld) {
        // println!("on_wake_up({:?})", id);
        for handler in &self.on_wake_up {
            handler(&id, world);
        }
    }

}