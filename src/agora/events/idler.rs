use super::*;

pub struct IdlerHandler;

impl IdlerHandler {

    pub fn sleep(id: &GenerationalId, world: &mut AgoraWorld) {
        if let Some(_unit) = world.units.get_mut(id) {
            if let Some(idler) = world.idlers.get(id) {
                world.actions.schedule(&idler.wait_time(&mut world.random), AgoraSystem::UnitWakeUp, id.clone());
            }
        }
    }

}