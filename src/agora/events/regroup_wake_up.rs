use super::*;
use crate::nj::pathfinding;

pub struct RegroupWakeUp;

impl RegroupWakeUp {

    pub fn wake_up(id: &GenerationalId, world: &mut AgoraWorld) {
        if let Some(unit) = world.units.get_mut(&id) {
            if let Some(regroup_location) = world.regroup_position.get(&id) {
                if let Some(idler) = world.idlers.get(&id) {
                    let target = regroup_location.random_location(&mut world.random);
                    let result = pathfinding::find_path(unit.position, target, &world.tiles, |tile| {
                        tile.terrain_flags.contains(&unit.terrain_flags)
                    }); 
                    if let Some(mut path) = result {
                        // println!("Unit({:?}): Found path: {:?}", id, path);
                        if let Some(position) = path.pop() {
                            let arrival_delay = pathfinding::dist(&unit.position, &position) as u8 * unit.travel_time;
        
                            world.tiles.get_mut(unit.position.0,unit.position.1).entities.remove(&id);
                            world.tiles.get_mut(position.0, position.1).entities.insert(id.clone());
        
                            unit.previous_position = unit.position;
                            unit.position = position;
                            unit.arrival_time = world.actions.current_tick() + arrival_delay as u64;
                            unit.departure_time = world.actions.current_tick();
                            unit.path = path;
                            world.actions.schedule(&arrival_delay, AgoraSystem::UnitStep, id.clone());
                        } else {
                            world.actions.schedule(&idler.wait_time(&mut world.random), AgoraSystem::UnitWakeUp, id.clone());
                        }
                    } else {
                        // println!("Unit({:?}): Sleeping (no path found)", id);
                        world.actions.schedule(&idler.wait_time(&mut world.random), AgoraSystem::UnitWakeUp,id.clone());
                    }
                }
            }
        }
    }

}