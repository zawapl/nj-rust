use rand::rngs::StdRng;

#[derive(Default,Debug)]
pub struct Idler {
    pub wait_time: u8
}

impl Idler {

    pub fn wait_time(&self, _random: &mut StdRng) -> u8 {
        return self.wait_time;
    }

}