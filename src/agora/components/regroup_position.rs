use rand::RngCore;
use std::convert::TryFrom;
use rand::rngs::StdRng;

#[derive(Debug)]
pub struct RegroupPosition {
    pub x: u16,
    pub y: u16
}

impl Default for RegroupPosition {

    fn default() -> Self {
        let x = u16::max_value();
        let y = u16::max_value();
        return Self{x,y};
    }

}

impl RegroupPosition {

    pub fn random_location(&self, random: &mut StdRng) -> (u16,u16) {
        let rx = self.x as i32;
        let ry = self.y as i32;
        let mut distance = (random.next_u32() & 0x7) as f64;
        let angle = random.next_u32() as f64;

        loop {
            let dx = (distance * angle.cos()) as i32;
            let dy = (distance * angle.sin()) as i32;

            let tx = rx + dx;
            let ty = ry + dy;

            if let Ok(target_x) = u16::try_from(tx) {
                if let Ok(target_y) = u16::try_from(ty) {
                    return (target_x, target_y);
                }
            }

            distance -= 1.0;

            debug_assert!(distance >= 0.0, "Failed to find a random location for {:?}", self);
        }
    }

}