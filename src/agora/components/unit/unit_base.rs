pub struct UnitBase {
    pub x: u16,
    pub y: u16,
    pub arrival_time: u64
}