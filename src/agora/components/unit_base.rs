use crate::agora::tiles::TerrainFlags;

#[derive(Debug)]
pub struct UnitBase {
    pub terrain_flags: TerrainFlags,
    pub travel_time: u8,
    pub path: Vec<(u16, u16)>,
    pub position: (u16, u16),
    pub arrival_time: u64,
    pub previous_position: (u16, u16),
    pub departure_time: u64,
}

impl Default for UnitBase {
    fn default() -> Self {
        return UnitBase {
            terrain_flags: TerrainFlags::EMPTY,
            travel_time: u8::max_value(),
            path: Vec::new(),
            position: (u16::max_value(), u16::max_value()),
            arrival_time: 0,
            previous_position: (u16::max_value(), u16::max_value()),
            departure_time: 0,
        };
    }
}
