use crate::nj::GenerationalId;

#[derive(Default,Debug)]
pub struct Spawner {
    pub units: Vec<GenerationalId>,
    pub delay: u8,
    pub max_count: u8,
    pub unit_template: SpawnerUnitTemplate
}

#[derive(Debug,Copy,Clone)]
pub enum SpawnerUnitTemplate {
    None,
    Wolf(u16,u16)
}

impl Default for SpawnerUnitTemplate {

    fn default() -> Self {
        return SpawnerUnitTemplate::None;
    }

}