use super::*;

use std::sync::Mutex;
use std::sync::Arc;
use std::time::SystemTime;
use std::time::Duration;

use tonic::{Request, Response, Status};

use grpc_agora::engine_service_server::*;
use grpc_agora::*;


pub mod grpc_agora {
    tonic::include_proto!("agora");
}

#[derive(Debug)]
pub struct AgoraServer {
    pub world: Arc<Mutex<AgoraWorld>>,
    pub props: Arc<Mutex<ServerProps>>
}

#[derive(Debug)]
pub struct ServerProps {
    pub rate: u64
}

#[tonic::async_trait]
impl EngineService for AgoraServer {

    async fn join(&self, request: Request<GameRequest>) -> Result<Response<WorldState>, Status> {
        println!("Request={:?}", request.into_inner());

        let mut world = self.world.lock().unwrap();

        let width = world.tiles.width as u32;
        let height = world.tiles.height as u32;
        let updates = Some(Self::updates(&mut world));

        let world_state = WorldState {width, height, updates};
        return Ok(Response::new(world_state));
    }

    async fn update(&self, request: Request<Commands>) -> Result<Response<Updates>, Status> {
        let commands = request.into_inner();

        println!("Update {:?}", commands);

        let mut world = self.world.lock().unwrap();
        
        let mut command_responses = vec!();

        {
            let mut guard = self.props.lock().unwrap();
            for set_game_speed in commands.set_game_speed_command {
                println!("Changing rate to {}", set_game_speed.new_rate);
                (*guard).rate = set_game_speed.new_rate;
                command_responses.push(CommandResponse{
                    command_id: set_game_speed.command_id,
                    command_status: 0
                });
                println!("Changed rate to {}", set_game_speed.new_rate);
            }
        }

        let mut updates = Self::updates(&mut world);

        updates.command_responses = command_responses;

        return Ok(Response::new(updates));
    }

}

impl AgoraServer {

    pub fn new(world: AgoraWorld) -> Self {
        return AgoraServer {
            world: Arc::new(Mutex::new(world)),
            props: Arc::new(Mutex::new(ServerProps{
                rate: 3600000
            }))
        };
    }

    pub fn clone(&self) -> Self {
        return Self {
            world: self.world.clone(),
            props: self.props.clone()
        }
    }
    
    fn updates(world: &mut AgoraWorld) -> Updates {

        let current_time = world.actions.current_tick();

        let command_responses = vec!();

        let mut tiles = vec!();

        world.tiles.read_changes(0, &mut |(x,y), tile|{
            let data = Some(TileData{
                terrain_flags: tile.terrain_flags.0 as u32
            });
            let position = Some(Position {
                x: *x as u32,
                y: *y as u32
            });
            tiles.push(Tile{position, data});
        });

        let mut units = Vec::new();

        world.units.read_changes(0, &mut |id, unit|{
            let index = id.index();
            let position = Some(Position {
                x: unit.position.0 as u32,
                y: unit.position.1 as u32
            });
            let arrival_time = unit.arrival_time;
            let departure_time = unit.departure_time as u64;
            let previous_position = Some(Position {
                x: unit.previous_position.0 as u32,
                y: unit.previous_position.1 as u32
            });
            let mut path = vec!();

            {
                let mut previous_arrival_time = arrival_time;
                let mut previous_position = &unit.position;
                for pos in unit.path.iter().rev().into_iter() {
                    let position = Some(Position{
                        x: pos.0 as u32,
                        y: pos.1 as u32
                    });
                    let arrival_time = ((nj::pathfinding::dist(pos,&previous_position) * unit.travel_time as u32) as u64) + previous_arrival_time;
                    path.push(PathSegment{position,arrival_time});
                    previous_arrival_time = arrival_time;
                    previous_position = &pos;
                }
            }

            units.push(UnitComponent{
                index,
                position,
                path,
                arrival_time,
                departure_time,
                previous_position
            });
        });

        println!("Units changes: {:?}", units);
        println!("Units state: {:?}", world.units);

        let mut identifier_states = Vec::new();

        world.allocator.read_changes(0, &mut |index, template|{
            let type_id = *template as u32;
            identifier_states.push(IdentifierState{index, type_id});
        });

        return Updates{current_time, command_responses, tiles, identifier_states, units};

    }
}

pub fn run_server(server: AgoraServer) {
    let event_listeners = EventListeners::new();

    let mutex = server.world.as_ref();

    loop {
        let tick_start = SystemTime::now();
        {
            let guard = mutex.lock();
            let mut world = guard.unwrap();
            println!("#### Tick={:?}", world.actions.current_tick());
            let mut updates = world.actions.tick();
            for (system,ids) in updates.iter_mut() {
                system.process(ids.drain(..), &event_listeners, &mut world);
            }
            world.actions.push_empty_frames_update(updates);
        }
        let mut waiting = true;
        while waiting {
            if let Ok(elapsed) = tick_start.elapsed() {
                let elapsed_milis = elapsed.as_millis() as u64;
                let guard = server.props.lock().unwrap();
                let rate = (*guard).rate;
                if elapsed_milis <= rate {
                    let waiting_time = std::cmp::min(100, rate - elapsed_milis);
                    std::thread::sleep(Duration::from_millis(waiting_time));
                } else {
                    waiting = false;
                }
            } else {
            }
        }
    }
}

pub fn create_world() -> AgoraWorld {
    println!("Creating world");

    let mut world = AgoraWorld::new(100,100);

    agora::initialisers::wolf_spawner::new(50, 50, &mut world);

    println!("World populated");

    return world;
}